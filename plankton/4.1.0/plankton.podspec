Pod::Spec.new do |s|
    s.name         = "plankton"
    s.version      = "4.1.0"
    s.summary      = "IOS version of plankton"
    s.description  = "Plankton implements admob, firebase and other needed SDKs and provides APIs to unity for working with these SDKs"
    s.homepage     = "https://gitlab.com/databite/plankton-ios-temp.git"
    s.license = { :type => 'Copyright', :text => <<-LICENSE
                   Copyright 2018
                   Permission is granted to...
                  LICENSE
                }
    s.author       = { "Movahed708" => "am.movahedian1998@gmail.com" }
    s.source       = { :git => "https://gitlab.com/databite/plankton-ios-temp.git", :tag => "#{s.version}" }
    s.source_files = "plankton/**/*.swift", "plankton/**/*.h", "plankton/**/*.m"
    s.resources = "plankton/**/*.h", "plankton/**/*.m", "plankton/**/*.a"
    s.platform = :ios
    s.swift_version = "4.2"
    s.ios.deployment_target  = '15.0'
    s.static_framework = true
    s.dependency "Google-Mobile-Ads-SDK", "~> 10.14.0"
    s.dependency "AppLovinSDK", "~> 12.1.0"
    s.dependency "AppLovinSDK"
    s.dependency "GoogleMobileAdsMediationFacebook"
    s.dependency "FirebaseAnalytics"
    s.dependency "FirebaseRemoteConfig"
    s.dependency "AppsFlyer-AdRevenue"
    s.dependency "YandexMobileMetrica"
    s.xcconfig = { 'OTHER_LDFLAGS' => '-undefined dynamic_lookup' }
    s.pod_target_xcconfig = {
            'OTHER_LDFLAGS' => '-undefined dynamic_lookup',
            'IPHONEOS_DEPLOYMENT_TARGET' => '15.0',
    }
    s.compiler_flags = '-undefined dynamic_lookup'
end
